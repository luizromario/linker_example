#include <dynamic_lib.h>
#include <static_lib.h>
#include <stdio.h>

int main()
{
    printf("about to do things from executable\n");
    do_things();

    printf("about to print thing from executable (should print \"new static lib\"): ");
    print_thing();

    return 0;
}
