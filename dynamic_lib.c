#include <dynamic_lib.h>
#include <static_lib.h>
#include <stdio.h>

void do_things()
{
    printf("about to print thing from dynamic lib (should print \"old static lib\"): ");
    print_thing();
}
